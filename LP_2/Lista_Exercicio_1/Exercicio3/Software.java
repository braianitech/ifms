package Exercicio3;


public class Software extends Produto {
    int numVersao;

    public int getNumVersao() {
        return numVersao;
    }

    public void setNumVersao(int numVersao) {
        this.numVersao = numVersao;
    }
    @Override
    public String toString(){
        return "Id: "+idProduto+", descrição: "+descricao+", preço: "+precoUnidade+" e versão: "+numVersao;
    }
}
