package lojacarro;

/**
 * @author braiani
 */
public class LojaCarro {

    public static void main(String[] args) {
        
        String nome = "Felipe Braiani" ;
        int idade = 25;
        int cpf = 0333112;
        int rg = 12345;
        String dono = nome;
        String marca = "Fiat";
        int ano = 2004;
        double valor = 16000;
        Pessoa pessoa = new Pessoa();
        Automovel carro = new Automovel();
        
        pessoa.setNome(nome);
        pessoa.setIdade(idade);
        pessoa.setCPF(cpf);
        pessoa.setRG(rg);
        carro.setDono(dono);
        carro.setMarca(marca);
        carro.setAno(ano);
        carro.setValor(valor);
        
        System.out.println("------- Pessoa ------");
        System.out.println("Nome: " + pessoa.getNome() +
                ", idade: " + pessoa.getIdade() + ", CPF: " + pessoa.getCPF() +
                " e RG: " + pessoa.getRG());
        System.out.println("------- Pessoa ------");
        System.out.println("Dono: " + carro.getDono() + ", marca: " + carro.getMarca()
        + ", ano: " + carro.getAno() + " e valor: " + carro.getValor());
    }
    
}