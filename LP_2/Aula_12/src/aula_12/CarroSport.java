package aula_12;
/**
 * @author braiani Criado em 29/05/2017
 */
public class CarroSport extends VeiculoAlugado {
    float velMaxima;

    public CarroSport(float velMaxima, int codigo, int valor) {
        super(codigo, valor);
        this.velMaxima = velMaxima;
    }

    public CarroSport() {
    }

    public float getVelMaxima() {
        return velMaxima;
    }

    public void setVelMaxima(float velMaxima) {
        this.velMaxima = velMaxima;
    }
    
    @Override
    public String imprimirDados(){
        return "O carro sport tem velocidade máxima de "+ velMaxima +" Km/h e valor de R$ "+
                super.getValor();
    }
}
