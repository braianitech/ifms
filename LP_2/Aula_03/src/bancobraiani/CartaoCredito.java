package bancobraiani;

public class CartaoCredito {
    private int numero;
    private String validadeAno;
    private Cliente cliente;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getValidadeAno() {
        return validadeAno;
    }

    public void setValidadeAno(String validadeAno) {
        this.validadeAno = validadeAno;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
   @Override
   public String toString(){
       return "\nNúmero do cartão: "+ numero +", validade do cartão: "+ validadeAno;
   }
    
}
