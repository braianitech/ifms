package aula_09_calculadora;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author braiani
 */
public class Aula_09_Calculadora {

    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();
        Scanner entrada = new Scanner(System.in);
        boolean continuar;
        float resultado;
        int menu;

        System.out.println("/---- Bem vindo a Caluladora BrTec ----\\");
        do {
            try {
                System.out.println("Digite o primeiro número");
                calculadora.setN1(entrada.nextFloat());
                System.out.println("Digite o segundo número:");
                calculadora.setN2(entrada.nextFloat());
                System.out.println("Digite a opção de calculo:");
                System.out.println("1 - Somar");
                System.out.println("2 - Subtrair");
                System.out.println("3 - Multiplicar");
                System.out.println("4 - Dividir");
                menu = entrada.nextInt();
                switch (menu) {
                    case 1:
                        resultado = calculadora.somar();
                        break;
                    case 2:
                        resultado = calculadora.subtrair();
                        break;
                    case 3:
                        resultado = calculadora.multiplicar();
                        break;
                    case 4:
                        resultado = calculadora.dividir();
                        break;
                    default:
                        resultado = 0;
                        break;
                }
                System.out.println("O resultado é: " + resultado);
            } catch (ArithmeticException e) {
                System.out.println("Impossível divisão por zero!");
            } catch (InputMismatchException ex) {
                System.out.println("Digite apenas números!");
            }finally{
                entrada.nextLine();
                System.out.println("|------------------------------------------------|");
                System.out.println("Deseja continuar calculando? (1- sim / 2 - não)");
                continuar = (entrada.nextInt() == 1);
            }
        } while (continuar);
    }
    
}
