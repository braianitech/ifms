package aula_12;

import java.util.ArrayList;

/**
 * @author braiani
 */
public class Aula_12 {

    public static void main(String[] args) {
        ArrayList<Caminhao> caminhaoList = new ArrayList<>();
        ArrayList<CarroPasseio> carroPList = new ArrayList<>();
        ArrayList<CarroSport> carroSList = new ArrayList<>();
        ArrayList<Cliente> clienteList = new ArrayList<>();
        ArrayList<ContratoAluguel> contratoList = new ArrayList<>();
        ArrayList<ContratoAluguel> contratoListAux = new ArrayList<>();
        ArrayList<VeiculoAlugado> veiculoAlugados = new ArrayList<>();
        ArrayList<CompanhiaAluguelVeiculo> companhiaList = new ArrayList<>();
        
        //Cadastros de Caminhão, Carro Passei, Carro Sport e Clientes
        caminhaoList = cadCaminhao(caminhaoList);
        carroPList = cadCarroPasseio(carroPList);
        carroSList = cadCarroSport(carroSList);
        clienteList = cadCliente(clienteList);
        
        //Criando os Contratos de Alugueis]
        veiculoAlugados.add(carroSList.get(0));
        veiculoAlugados.add(carroPList.get(1));
        contratoList.add(cadContratoAluguel("10/02/2017", clienteList.get(0), veiculoAlugados));
        
        veiculoAlugados = new ArrayList<>();
        veiculoAlugados.add(caminhaoList.get(0));
        veiculoAlugados.add(caminhaoList.get(1));
        contratoList.add(cadContratoAluguel("15/03/2017", clienteList.get(1), veiculoAlugados));
        
        veiculoAlugados = new ArrayList<>();
        veiculoAlugados.add(carroSList.get(1));
        veiculoAlugados.add(carroPList.get(0));
        contratoList.add(cadContratoAluguel("20/04/2017", clienteList.get(2), veiculoAlugados));
        
        //Criando as Companhias de Alugueis de Veículos
        contratoListAux.add(contratoList.get(0));
        companhiaList.add(cadCompanhiaAluguel("Localiza", "999.888.777", contratoListAux));
        
        contratoListAux = new ArrayList<>();
        contratoListAux.add(contratoList.get(1));
        companhiaList.add(cadCompanhiaAluguel("Rent a Car", "888.777.666", contratoListAux));
        
        //Imprimir Resultados
        System.out.println("--- Companhias ---");
        for (int i = 0; i < companhiaList.size(); i++) {
            System.out.println(companhiaList.get(i).imprimirDados() + "\n");
        }
        System.out.println("--- Clientes ---");
        for(int i = 0; i < clienteList.size(); i++){
            System.out.println(clienteList.get(i).imprimirDados() + "\n");
        }
        
        System.out.println("--- Veículos ---");
        for(int i = 0; i < veiculoAlugados.size(); i++){
            System.out.println(veiculoAlugados.get(i).imprimirDados() + "\n");
        }
    }
    
    private static ArrayList<Caminhao> cadCaminhao(ArrayList<Caminhao> caminhaoList){
        Caminhao cm1 = new Caminhao(234, 111, 120000);
        Caminhao cm2 = new Caminhao(452, 222, 240000);
        caminhaoList.add(cm1);
        caminhaoList.add(cm2);
        return caminhaoList;
    }
    
    private static ArrayList<CarroPasseio> cadCarroPasseio(ArrayList<CarroPasseio> carroPList){
        CarroPasseio cp1 = new CarroPasseio(4, 3330, 35000);
        CarroPasseio cp2 = new CarroPasseio(5, 444, 40000);
        carroPList.add(cp1);
        carroPList.add(cp2);
        return carroPList;
    }
    
    private static ArrayList<CarroSport> cadCarroSport(ArrayList<CarroSport> carroSList){
        CarroSport cs1 = new CarroSport(200, 555, 70000);
        CarroSport cs2 = new CarroSport(250, 666, 90000);
        carroSList.add(cs1);
        carroSList.add(cs2);
        return carroSList;
    }
    
    private static ArrayList<Cliente> cadCliente(ArrayList<Cliente> clienteList){
        Cliente c1 = new Cliente("João", "111.222.333");
        Cliente c2 = new Cliente("Maria", "222.333.444");
        Cliente c3 = new Cliente("Fernando", "333.444.555");
        clienteList.add(c1);
        clienteList.add(c2);
        clienteList.add(c3);
        return clienteList;
    }
    
    private static ContratoAluguel cadContratoAluguel(String data, Cliente cliente, ArrayList<VeiculoAlugado> veiculoAlugados){
        ContratoAluguel contratoAluguel = new ContratoAluguel(data, cliente, veiculoAlugados);
        return contratoAluguel;
    }
    
    private static CompanhiaAluguelVeiculo cadCompanhiaAluguel(String nome, String cnpj, ArrayList<ContratoAluguel> contratos){
        CompanhiaAluguelVeiculo companhia = new CompanhiaAluguelVeiculo(nome, cnpj, contratos);
        return companhia;
    }
}

        