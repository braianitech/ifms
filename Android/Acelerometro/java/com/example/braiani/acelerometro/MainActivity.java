package com.example.braiani.acelerometro;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView txtViewX, txtViewY, txtViewZ, txtViewDetalhes;
    private SensorManager mSensorManager;
    private Sensor mAcelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAcelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAcelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Float x = event.values[0];
        Float y = event.values[1];
        Float z = event.values[2];

        txtViewX.setText("Posição X: " + x.intValue() + ", Float: " + x);
        txtViewY.setText("Posição Y: " + y.intValue() + ", Float: " + y);
        txtViewZ.setText("Posição Z: " + z.intValue() + ", Float: " + z);

        if(y > -5 && y < 5){
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }else {
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }

        if(y < 0){
            if(x > 0)
                txtViewDetalhes.setText("Virando para ESQUERDA ficando INVERTIDO");
            if(x < 0)
                txtViewDetalhes.setText("Virando para DIREITA ficando INVERTIDO");
        }else {
            if(x > 0)
                txtViewDetalhes.setText("Virando para ESQUERDA ");
            if(x < 0)
                txtViewDetalhes.setText("Virando para DIREITA ");
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void findViews(){
        txtViewX = (TextView) findViewById(R.id.txtViewX);
        txtViewY = (TextView) findViewById(R.id.txtViewY);
        txtViewZ = (TextView) findViewById(R.id.txtViewZ);
        txtViewDetalhes = (TextView) findViewById(R.id.txtViewDetalhes);
    }
}
