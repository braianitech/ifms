package com.brt.braianitech.firebase;

/**
 * Created by Braiani on 13/06/2017.
 */

public class Pessoa {
    private String nome, endereco;

    public Pessoa(){

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}
