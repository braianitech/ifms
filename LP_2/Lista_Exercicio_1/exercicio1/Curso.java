package exercicio1;

import java.util.ArrayList;

public class Curso {

    private int codCurso;
    private String nome;
    private int cargaHoraria;
    private ArrayList<Turma> Turmas;

    public int getCodCurso() {
        return codCurso;
    }

    public void setCodCurso(int codCurso) {
        this.codCurso = codCurso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public ArrayList<Turma> getTurmas() {
        return Turmas;
    }

    public void setTurmas(ArrayList<Turma> Turmas) {
        this.Turmas = Turmas;
    }
}
