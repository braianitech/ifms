package aula_12;

import java.util.ArrayList;

/**
 * @author braiani Criado em 29/05/2017
 */
public class CompanhiaAluguelVeiculo {
    String nome, cnpj;
    ArrayList<ContratoAluguel> contratoAluguel;

    public CompanhiaAluguelVeiculo() {
    }

    public CompanhiaAluguelVeiculo(String nome, String cnpj, ArrayList<ContratoAluguel> contratoAluguel) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.contratoAluguel = contratoAluguel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public ArrayList<ContratoAluguel> getContratoAluguel() {
        return contratoAluguel;
    }

    public void setContratoAluguel(ArrayList<ContratoAluguel> contratoAluguel) {
        this.contratoAluguel = contratoAluguel;
    }
    
    public String imprimirDados(){
        return "A companhia " + nome + ", CNPJ " + cnpj + " possui os seguintes contratos: \n"+
                contratoAluguel.toString();
    }
}
