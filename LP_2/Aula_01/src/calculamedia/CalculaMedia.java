package calculamedia;

import java.util.Scanner;

/**
 * @author braiani
 */
public class CalculaMedia {

    public static void main(String[] args) {
        Medias objMedias = new Medias();
        SituacaoAluno objSituacao = new SituacaoAluno();
        Scanner sc = new Scanner(System.in);

        double n1, n2, n3;

        System.out.println("Bem vindo ao sistema de calculo de Médias!\n");

//        for(i = 1; i <= 4; i++){
//            System.out.println("Digite a ");
//                    
//        }

        System.out.println("Digite a 1a nota: ");
        n1 = sc.nextInt();
        System.out.println("Digite a 2a nota: ");
        n2 = sc.nextInt();
        System.out.println("Digite a 3a nota: ");
        n3 = sc.nextInt();

        double media = objMedias.calcular(n1, n2, n3);

        boolean situacao = objSituacao.situacao(media);
        
        if(situacao){
            System.out.printf("A média do aluno é: %.1f", media);
            System.out.println("\nO aluno está aprovado!");
        }else{
            System.out.printf("A média do aluno é: %.1f", media);
            System.out.println("\nO aluno está reprovado!");
        }
    }
    
}
