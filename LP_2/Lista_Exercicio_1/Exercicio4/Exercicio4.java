package Exercicio4;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercicio4 {

    static Scanner entrada = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<Agencia> agencias = new ArrayList<>();
        ArrayList<Gerente> gerentes = new ArrayList<>();
        ArrayList<Gerente> auxGerentes = new ArrayList<>();
        ArrayList<ContaCorrente> contaCorrentes = new ArrayList<>();
        ArrayList<ContaPoupanca> contaPoupancas = new ArrayList<>();
        ArrayList<PessoaFisica> pessoaFisicas = new ArrayList<>();
        ArrayList<PessoaJuridica> pessoaJuridicas = new ArrayList<>();

        //Cadastro de Pessoa Física
        System.out.println("=> Cadastro de Pessoa Física <=");
        for (int i = 0; i < 2; i++) {
            pessoaFisicas.add(cadPessoaFisica(i));
        }

        //Cadastro de Pessoa Jurídica
        System.out.println("=> Cadastro de Pessoa Jurídica <=");
        for (int i = 0; i < 2; i++) {
            pessoaJuridicas.add(cadPessoaJuridica(i));
        }

        //Cadastro de Gerente
        System.out.println("=> Cadastro de Gerente <=");
        for (int i = 0; i < 2; i++) {
            gerentes.add(cadGerente(i));
        }

        //Cadastro Agencias
        System.out.println("=> Cadastro de Agências <=");
        for (int i = 0; i < 2; i++) {
            agencias.add(cadAgencia(i));
        }

        //Cadastro Conta Corrente
        System.out.println("=> Cadastro de Contas Corrente <=");
        for (int i = 0; i < 2; i++) {
            contaCorrentes.add(cadContaCorrente(i));
        }

        //Cadastro Conta Poupança
        System.out.println("=> Cadastro de Contas Poupança <=");
        for (int i = 0; i < 2; i++) {
            contaPoupancas.add(cadContaPoupanca(i));
        }
        //Vinculação das Contas a Agências
        System.out.println("Vinculando contas da agência " + agencias.get(0).getNomeAgencia() + "...");
        agencias.get(0).setArrayContaCorrente(contaCorrentes);
        System.out.println("Vinculando contas da agência " + agencias.get(1).getNomeAgencia() + "...");
        agencias.get(1).setArrayContaPoupanca(contaPoupancas);
        
        //Vinculação Conta a pessoa
        System.out.println("Vinculando contas aos titulares...");
        contaCorrentes.get(0).setTitular(pessoaJuridicas.get(0));
        contaCorrentes.get(1).setTitular(pessoaJuridicas.get(1));
        contaPoupancas.get(0).setTitular(pessoaFisicas.get(0));
        contaPoupancas.get(1).setTitular(pessoaFisicas.get(1));
        
        //Vinculação dos gerentes
        System.out.println("Vinculando os gerentes as agências...");
        auxGerentes.add(gerentes.get(0));
        agencias.get(0).setArrayGerentes(auxGerentes);
        auxGerentes = new ArrayList<>();
        auxGerentes.add(gerentes.get(1));
        agencias.get(1).setArrayGerentes(auxGerentes);
        
        //Exibir o relatório
        System.out.println("========== RELATÓRIO ==========");
        for (int i = 0; i < 2; i++) {
            System.out.println("");
            System.out.println("Informações da "+(i +1)+"ª agência....");
            System.out.println("Código da Agência: " + agencias.get(i).getCodAgencia()
            + ", nome da agência: "+ agencias.get(i).getNomeAgencia());
            System.out.println("Essa agencia possui os seguintes gerentes vinculados: "
            + agencias.get(i).getArrayGerentes().toString());
            System.out.println("Essa agencia possui as seguintes contas vinculados: ");
            if (agencias.get(i).getArrayContaCorrente() != null) {
                for (int j = 0; j < 2; j++) {
                    System.out.println("número: " + agencias.get(i).getArrayContaCorrente().get(j).getNumero()+
                            ", data de abertura: "+ agencias.get(i).getArrayContaCorrente().get(j).getDataAbertura());
                    System.out.println("Essa conta possui o seguinte titular:");
                    System.out.println(agencias.get(i).getArrayContaCorrente().get(j).getTitular().toString());
                    System.out.println("O limite do cheque especial dessa conta é: "
                            +agencias.get(i).getArrayContaCorrente().get(j).getLimiteChequeEspecial());
                }
            }else{
                for (int j = 0; j < 2; j++) {
                    System.out.println("número: " + agencias.get(i).getArrayContaPoupanca().get(j).getNumero()+
                            ", data de abertura: "+ agencias.get(i).getArrayContaPoupanca().get(j).getDataAbertura());
                    System.out.println(agencias.get(i).getArrayContaPoupanca().get(j).getTitular().toString());
                    System.out.println("O dígito verificador dessa conta é: "
                            +agencias.get(i).getArrayContaPoupanca().get(j).getDigitoVerif());
                }
            }
        }
    }

    //Função para cadastro de pessoa física
    public static PessoaFisica cadPessoaFisica(int num) {
        PessoaFisica pessoaFisica = new PessoaFisica();
        System.out.println("----------------------------------------------------------");
        System.out.println("Cadastrando a " + (num + 1) + "ª pessoa física...");
        System.out.println("Digite o nome da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setNome(entrada.nextLine());
        System.out.println("Digite o CPF da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setCpf(entrada.nextLine());
        System.out.println("Digite a idade da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setIdade(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite o endereço da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setEndereco(entrada.nextLine());
        System.out.println("Digite o telefone da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setTelefone(entrada.nextLine());
        System.out.println("Digite a cidade da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setCidade(entrada.nextLine());
        System.out.println("Digite Estado da " + (num + 1) + "ª pessoa física:");
        pessoaFisica.setEstado(entrada.nextLine());
        return pessoaFisica;
    }

    //Função para cadastro de pessoa jurídica
    public static PessoaJuridica cadPessoaJuridica(int num) {
        PessoaJuridica pessoaJuridica = new PessoaJuridica();
        System.out.println("----------------------------------------------------------");
        System.out.println("Cadastrando a " + (num + 1) + "ª pessoa jurídica...");
        System.out.println("Digite o nome da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setNome(entrada.nextLine());
        System.out.println("Digite o CNPJ da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setCnpj(entrada.nextLine());
        System.out.println("Digite o tempo de atividade da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setIdade(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite o endereço da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setEndereco(entrada.nextLine());
        System.out.println("Digite o telefone da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setTelefone(entrada.nextLine());
        System.out.println("Digite a cidade da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setCidade(entrada.nextLine());
        System.out.println("Digite Estado da " + (num + 1) + "ª pessoa jurídica:");
        pessoaJuridica.setEstado(entrada.nextLine());
        return pessoaJuridica;
    }

    //Função para cadastro de gerente
    public static Gerente cadGerente(int num) {
        Gerente gerente = new Gerente();
        System.out.println("----------------------------------------------------------");
        System.out.println("Cadastrando o " + (num + 1) + "º gerente...");
        System.out.println("Digite o nome do " + (num + 1) + "º gerente:");
        gerente.setNome(entrada.nextLine());
        System.out.println("Digite o Código funcional do " + (num + 1) + "º gerente:");
        gerente.setCodFuncional(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite a idade do " + (num + 1) + "º gerente:");
        gerente.setIdade(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite o endereço do " + (num + 1) + "º gerente:");
        gerente.setEndereco(entrada.nextLine());
        System.out.println("Digite o telefone do " + (num + 1) + "º gerente:");
        gerente.setTelefone(entrada.nextLine());
        System.out.println("Digite a cidade do " + (num + 1) + "º gerente:");
        gerente.setCidade(entrada.nextLine());
        System.out.println("Digite Estado do " + (num + 1) + "º gerente:");
        gerente.setEstado(entrada.nextLine());
        return gerente;
    }

    //Função para cadastro de Conta Corrente
    public static ContaCorrente cadContaCorrente(int num) {
        ContaCorrente contaCorrente = new ContaCorrente();
        System.out.println("---------------------------------------------------------");
        System.out.println("Cadastrando a " + (num + 1) + "ª conta corrente...");
        System.out.println("Digite o número da " + (num + 1) + "ª conta corrente:");
        contaCorrente.setNumero(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite a data de abertura dessa conta corrente");
        contaCorrente.setDataAbertura(entrada.nextLine());
        System.out.println("Digite o limite do cheque especial para essa conta:");
        contaCorrente.setLimiteChequeEspecial(entrada.nextDouble());

        return contaCorrente;
    }

    //Função para cadastro de Conta Poupança
    public static ContaPoupanca cadContaPoupanca(int num) {
        ContaPoupanca contaPoupanca = new ContaPoupanca();
        System.out.println("---------------------------------------------------------");
        System.out.println("Cadastrando a " + (num + 1) + "ª conta poupança...");
        System.out.println("Digite o número da " + (num + 1) + "ª conta poupança:");
        contaPoupanca.setNumero(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite a data de abertura dessa conta poupança");
        contaPoupanca.setDataAbertura(entrada.nextLine());
        System.out.println("Digite o código verificador dessa conta poupança:");
        contaPoupanca.setDigitoVerif(entrada.nextInt());

        return contaPoupanca;
    }

    //Função para cadastro de Agência
    public static Agencia cadAgencia(int num) {
        Agencia agencia = new Agencia();
        System.out.println("----------------------------------------------------------");
        System.out.println("Cadastrando a " + (num + 1) + "ª agência....");
        System.out.println("Digite o código da " + (num + 1) + "ª agência:");
        agencia.setCodAgencia(entrada.nextInt());
        entrada.nextLine();
        System.out.println("Digite o nome da " + (num + 1) + "ª agência:");
        agencia.setNomeAgencia(entrada.nextLine());
        return agencia;
    }
}