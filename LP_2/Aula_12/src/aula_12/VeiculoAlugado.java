package aula_12;
/**
 * @author braiani Criado em 29/05/2017
 */
public class VeiculoAlugado {
    int codigo, valor;

    public VeiculoAlugado() {
    }

    public VeiculoAlugado(int codigo, int valor) {
        this.codigo = codigo;
        this.valor = valor;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
    public String imprimirDados(){
        return "O código do veículo é: "+ codigo +", e o valor: "+ valor;
    }

    @Override
    public String toString() {
        return "codigo: " + codigo + ", valor: " + valor;
    }
    
}
