package exercicio1;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercicio1 {

    static Scanner entrada = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<Aluno> listaAluno = new ArrayList<>();
        ArrayList<Aluno> auxListaAluno = new ArrayList<>();
        ArrayList<Turma> listaTurma = new ArrayList<>();
        ArrayList<Turma> auxListaTurma = new ArrayList<>();
        ArrayList<Curso> listaCurso = new ArrayList<>();

        //Alunos
        System.out.println("---- Cadastrando Alunos ----");
        for (int i = 0; i < 3; i++) {
            listaAluno.add(cadAluno(i));
        }

        //Turmas
        System.out.println("---- Cadastrando Turmas ----");
        for (int i = 0; i < 3; i++) {
            listaTurma.add(cadTurma(i));
        }

        //Cursos
        System.out.println("---- Cadastrando Cursos ----");
        for (int i = 0; i < 3; i++) {
            listaCurso.add(cadCurso(i));
        }
        
        //Vinculação dos estudantes conforme pedido pelo exercício
        //Curso 1 = Turmas 1 e 2
        auxListaTurma.add(listaTurma.get(0));
        auxListaTurma.add(listaTurma.get(1));
        listaCurso.get(0).setTurmas(auxListaTurma);
        
        //Curso 2 = Turma 3
        auxListaTurma = null;
        auxListaTurma = new ArrayList<>();
        auxListaTurma.add(listaTurma.get(2));
        listaCurso.get(1).setTurmas(auxListaTurma);
        
        //Curso 3 = Turmas 1 e 3
        auxListaTurma = null;
        auxListaTurma = new ArrayList<>();
        auxListaTurma.add(listaTurma.get(0));
        auxListaTurma.add(listaTurma.get(2));
        listaCurso.get(2).setTurmas(auxListaTurma);
        
        //Turma 1 = Alunos 1 e 2
        auxListaAluno.add(listaAluno.get(0));
        auxListaAluno.add(listaAluno.get(1));
        listaTurma.get(0).setAlunos(auxListaAluno);
        
        //Turma 2 = Aluno 3
        auxListaAluno = null;
        auxListaAluno = new ArrayList<>();
        auxListaAluno.add(listaAluno.get(2));
        listaTurma.get(1).setAlunos(auxListaAluno);
        
        //Turma 3 = Alunos 1 e 3
        auxListaAluno = null;
        auxListaAluno = new ArrayList<>();
        auxListaAluno.add(listaAluno.get(0));
        auxListaAluno.add(listaAluno.get(2));
        listaTurma.get(2).setAlunos(auxListaAluno);
        
        System.out.println("---- Exibindo Resultados ----");
        for (int i = 0; i < 3; i++) {
            System.out.println("---------------------------------------------------");
            System.out.println("O "+(i+1)+"º curso possui as seguintes informações:");
            //System.out.println(listaCurso.get(i).toString());
            System.out.println("Código: " + listaCurso.get(i).getCodCurso() + ", nome: " + listaCurso.get(i).getNome()
                    + ", Carga horária: " + listaCurso.get(i).getCargaHoraria());
            System.out.println("Este curso possui as seguintes turmas e alunos vinculadas:");
            for (Turma turma : listaCurso.get(i).getTurmas()) {
                System.out.println("Turma:");
                System.out.println("Código: " + turma.getCodigo() + ", descrição: " + turma.getDescricao());
                System.out.println("Alunos:");
                for (Aluno aluno : turma.getAlunos()) {
                    System.out.println("Nome: "+ aluno.getNome());
                }
            }
            System.out.println("---------------------------------------------------");
        }
    }

    public static Aluno cadAluno(int i) {
        Aluno aluno = new Aluno();
        System.out.println("Digie o nome do " + (i + 1) + "º aluno: ");
        aluno.setNome(entrada.nextLine());
        return aluno;
    }

    public static Turma cadTurma(int i) {
        Turma turma = new Turma();
        System.out.println("Digite o código da "+(i+1)+"ª turma: ");
        turma.setCodigo(entrada.nextInt());
        System.out.println("Digite a descrição da " + (i + 1) + "ª turma: ");
        turma.setDescricao(entrada.nextLine());
        return turma;
    }
    
    public static Curso cadCurso(int i) {
        Curso curso = new Curso();
        System.out.println("Digite o código do "+(i+1)+"º curso: ");
        curso.setCodCurso(entrada.nextInt());
        System.out.println("Digite o nome do " + (i + 1) + "º curso: ");
        curso.setNome(entrada.nextLine());
        System.out.println("Digite a carga horária do "+(i+1)+"º curso");
        curso.setCargaHoraria(entrada.nextInt());
        return curso;
    }
}
