package aula_09_calculadora;
/**
 * @author braiani
 */
public class Calculadora {
    private float n1;
    private float resultado;
    private float n2;
    
    public float getN1() {
        return n1;
    }

    public void setN1(float n1) {
        this.n1 = n1;
    }

    public float getN2() {
        return n2;
    }

    public void setN2(float n2) {
        this.n2 = n2;
    }

    public float getResultado() {
        return resultado;
    }

    public void setResultado(float resultado) {
        this.resultado = resultado;
    }
    
    public float somar(){
        resultado = n1 + n2;
        return resultado;
    }
    
    public float subtrair(){
        resultado = n1 - n2;
        return resultado;
    }
    
    public float multiplicar(){
        resultado = n1 * n2;
        return resultado;
    }
    
    public float dividir() throws ArithmeticException{
        if (n2 == 0) {
            ArithmeticException erro = new ArithmeticException();
            throw erro;
        }else{
            resultado = n1 / n2;
        }
        return resultado;
        //return (resultado = n1 / n2);
    }
}
