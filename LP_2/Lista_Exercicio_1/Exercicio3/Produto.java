package Exercicio3;


public class Produto {
    int idProduto;
    String descricao;
    int precoUnidade;

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPrecoUnidade() {
        return precoUnidade;
    }

    public void setPrecoUnidade(int precoUnidade) {
        this.precoUnidade = precoUnidade;
    }
    
}
