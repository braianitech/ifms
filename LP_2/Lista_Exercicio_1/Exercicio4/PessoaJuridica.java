package Exercicio4;


public class PessoaJuridica extends Pessoa{
    String cnpj;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
    @Override
    public String toString(){
        return "Nome: "+nome+", CNPJ: "+cnpj+", tempo de atividade: "+idade+", endereço: "+endereco
                +", telefone: "+telefone+", cidade: "+cidade+", estado: "+estado;
    }
}
