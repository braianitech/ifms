package com.brt.braianitech.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private EditText editTextNome, editTextEndereco;
    private TextView txtResultado;
    private Button btnCadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        encontrarViews();
        Firebase.setAndroidContext(this);
        btnCadastrar.setOnClickListener(ouvinte);
    }

    View.OnClickListener ouvinte = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            //Cria um objeto Firebase para utilizar na comunicação
            Firebase objFirebase = new Firebase(Config.FIREBASE_URL);

            //Cria as variáveis que recebem os valores do usuário
            String nome = editTextNome.getText().toString().trim();
            String endereco = editTextEndereco.getText().toString().trim();

            //Cria objeto para adicionar os valores do usuário
            Pessoa pessoa = new Pessoa();
            pessoa.setNome(nome);
            pessoa.setEndereco(endereco);

            //Limpar os campos de digitação para nova entrada de dados
            editTextEndereco.setText("");
            editTextNome.requestFocus();
            editTextNome.setText("");

            //Armazenar os valores no Firebase
            objFirebase.child("Pessoa").setValue(pessoa);

            //Recuperar os dados enviados ao Firebase
            objFirebase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapShot: dataSnapshot.getChildren()){
                        //Recuperando do Snapshot
                        Pessoa pessoa = postSnapShot.getValue(Pessoa.class);
                        //Adicionando a uma string
                        String resultado = "Nome: " + pessoa.getNome() +
                                            "\nEndereço: " + pessoa.getEndereco() + "\n\n";
                        txtResultado.setText(resultado);
                        txtResultado.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("A leitura falhou: " + firebaseError.getMessage());
                }
            });
        }
    };

    private void encontrarViews(){
        editTextNome = (EditText) findViewById(R.id.edTxtNome);
        editTextEndereco = (EditText) findViewById(R.id.edTxtEndereco);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);
    }
}