package aula_12;
/**
 * @author braiani Criado em 29/05/2017
 */
public class Caminhao extends VeiculoAlugado{
    float capacidadeKg;

    public Caminhao(float capacidadeKg, int codigo, int valor) {
        super(codigo, valor);
        this.capacidadeKg = capacidadeKg;
    }

    public Caminhao() {
    }

    public float getCapacidadeKg() {
        return capacidadeKg;
    }

    public void setCapacidadeKg(float capacidadeKg) {
        this.capacidadeKg = capacidadeKg;
    }

    @Override
    public String imprimirDados(){
        return "Caminhão com capacidade de "+ capacidadeKg +" Kg e valor de R$ "+
                super.getValor();
    }
}
