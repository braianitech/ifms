package bancobraiani;

import java.util.ArrayList;

public class Cliente {
    private String nome;
    private int id;
    private ArrayList<CartaoCredito> cartaoCredito;

    public ArrayList<CartaoCredito> getCartaoCredito() {
        return cartaoCredito;
    }

    public void setCartaoCredito(ArrayList<CartaoCredito> cartaoCredito) {
        this.cartaoCredito = cartaoCredito;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}
