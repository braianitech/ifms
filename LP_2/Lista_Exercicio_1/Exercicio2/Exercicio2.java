
package Exercicio2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Exercicio2 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        ArrayList<Personalidade> listaPersonalidade = new ArrayList<>();
        Kevin kevin = new Kevin();
        Random gerador = new Random();
        
        
        for (int i = 0; i < 5; i++) {
            Personalidade personalidade = new Personalidade();
            System.out.println("Digite a "+(i+1)+"ª personalidade: ");
            personalidade.setDescricao(entrada.next());
            listaPersonalidade.add(personalidade);
        }
        kevin.setArayPersonalidade(listaPersonalidade);
        int menu;
        int nPersonalidade;
        do {
            nPersonalidade = gerador.nextInt(4);
            System.out.println("--------------------------------------------------------------");
            System.out.println("A personalidade sorteada foi: "+kevin.getArayPersonalidade().get(nPersonalidade).getDescricao());
            System.out.println("--------------------------------------------------------------");
            System.out.println("Para sortear nova personalidade digite 1 e para fechar digite 2: ");
            menu = entrada.nextInt();
        } while (menu != 2);
    }
}
