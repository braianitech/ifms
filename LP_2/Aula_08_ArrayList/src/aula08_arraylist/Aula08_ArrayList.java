package aula08_arraylist;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author braiani
 */
public class Aula08_ArrayList {
    
    static Scanner entrada = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        ArrayList<Aluno> listaAluno = new ArrayList<>();
        ArrayList<Turma> listaTurma = new ArrayList<>();
        ArrayList<Curso> listaCurso = new ArrayList<>();
        int menu = 0;
        int arrayNum = 0;

        //Alunos
        System.out.println("Cadastrando Alunos...");
        cadAluno(listaAluno);

        //Turmas
        System.out.println("Cadastrando Turmas...");
        cadTurma(listaTurma);

        //Cursos
        System.out.println("Cadastrando Cursos...");
        cadCurso(listaCurso);
        
        do {            
            //System.out.println("---------------------------");
            System.out.println("Escolha uma das opções abaixo:");
            System.out.println("1) Zerar um arraylist");
            System.out.println("2) Verificar se um arraylist está vazio");
            System.out.println("3) Verificar tamanho de um array");
            System.out.println("4) Remover um objeto de um array, passando a posição");
            System.out.println("5) Substituir um objeto de um array, passando a posição");
            System.out.println("6) Exibir dados de um array");
            System.out.println("7) Sair");
            menu = entrada.nextInt();
            if (menu > 0 && menu < 7) {
                System.out.println("Com qual Array você quer trabalhar?");
                System.out.println("1 - Alunos");
                System.out.println("2 - Turmas");
                System.out.println("3 - Cursos");
                arrayNum = entrada.nextInt();
                if (menu == 5) {
                    switch (arrayNum) {
                        case 1:
                            substituirPosicao(listaAluno, arrayNum);
                            break;
                        case 2:
                            substituirPosicao(listaTurma, arrayNum);
                            break;
                        case 3:
                            substituirPosicao(listaCurso, arrayNum);
                            break;
                    }
                } else {
                    switch (arrayNum) {
                        case 1:
                            selecMenu(listaAluno, menu);
                            break;
                        case 2:
                            selecMenu(listaTurma, menu);
                            break;
                        case 3:
                            selecMenu(listaCurso, menu);
                            break;
                    }
                }
            }
        } while (menu != 7);
    }
    
    public static void selecMenu(ArrayList arrayList, int menu) {
        switch (menu) {
            case 1:
                zerarArray(arrayList);
                break;
            case 2:
                estaVazio(arrayList);
                break;
            case 3:
                tamanhoArray(arrayList);
                break;
            case 4:
                removePosicao(arrayList);
                break;
            case 6:
                exibirRelatorio(arrayList);
                break;
        }
    }
    
    public static void zerarArray(ArrayList arrayList){
        arrayList.clear();
        System.out.println("/------------------------------\\");
        System.out.println("O Array selecionado foi zerado com sucesso");
        System.out.println("/------------------------------\\");
    }
    
    public static void estaVazio(ArrayList arrayList){
        boolean vazio = arrayList.isEmpty();
        if (vazio) {
            System.out.println("/------------------------------\\");
            System.out.println("O Array selecionado está vazio!");
            System.out.println("/------------------------------\\");
        }else{
            System.out.println("/-----------------------------------\\");
            System.out.println("O Array selecionado não está vazio!");
            System.out.println("/-----------------------------------\\");
        }
    }
    
    public static void tamanhoArray(ArrayList arrayList){
        System.out.println("/-----------------------------------\\");
        System.out.println("O Array selecionado possui "+ arrayList.size() + " posições!");
        System.out.println("/------------------------------------\\");
        
    }
    
    public static void removePosicao(ArrayList arrayList){
        System.out.println("/------------------------------------\\");
        System.out.println("Qual posição você deseja remover do Array?");
        int posicao = entrada.nextInt();
        if (posicao > (arrayList.size() - 1)) {
            System.out.println("Impossível remover na posição selecionada...");
            System.out.println("/------------------------------------\\");
        } else {
            //arrayList.remove(posicao);
            System.out.println("Removendo... (" + arrayList.remove(posicao).toString() + ")");
            System.out.println("/------------------------------------\\");
        }
    }
    
    public static void substituirPosicao(ArrayList arrayList, int arrayNum) {
        int posicao;
        System.out.println("/------------------------------------\\");
        switch (arrayNum) {
            case 1:
                Aluno a1 = new Aluno();
                a1.setNome("Joãozinho");
                System.out.println("Qual posição substituir?");
                posicao = entrada.nextInt();
                if (posicao > (arrayList.size() - 1)) {
                    System.out.println("Impossível substituir na posição selecionada...");
                    System.out.println("/------------------------------------\\");
                } else {
                    arrayList.set(posicao, a1);
                    System.out.println(arrayList.toString());
                }
                break;
            case 2:
                Turma t1 = new Turma();
                t1.setCodigo(235);
                t1.setDescricao("Turma de Java");
                System.out.println("Qual posição substituir?");
                posicao = entrada.nextInt();
                if (posicao > (arrayList.size() - 1)) {
                    System.out.println("Impossível substituir na posição selecionada...");
                    System.out.println("/------------------------------------\\");
                } else {
                    arrayList.set(posicao, t1);
                    System.out.println(arrayList.toString());
                }
                break;
            case 3:
                Curso c1 = new Curso();
                c1.setCargaHoraria(398);
                c1.setCodCurso(15);
                c1.setNome("Curso de PHP");
                System.out.println("Qual posição substituir?");
                posicao = entrada.nextInt();
                if (posicao > (arrayList.size() - 1)) {
                    System.out.println("Impossível substituir na posição selecionada...");
                    System.out.println("/------------------------------------\\");
                } else {
                    arrayList.set(posicao, c1);
                    System.out.println(arrayList.toString());
                }
                break;
            default:
                break;
        }
    }
    
    public static void exibirRelatorio(ArrayList arrayList) {
        System.out.println("---- Exibindo Resultados ----");
        System.out.println(arrayList.toString());
        System.out.println("---------------------------------------------------");
    }

    public static void cadAluno(ArrayList<Aluno> listaAluno) {
        Aluno a1, a2, a3;
        a1 = new Aluno();
        a2 = new Aluno();
        a3 = new Aluno();
        
        a1.setNome("José Antonio");
        a2.setNome("Leticia Mouta");
        a3.setNome("Bruno Lima");
        
        listaAluno.add(a1);
        listaAluno.add(a2);
        listaAluno.add(a3);
    }

    public static void cadTurma(ArrayList<Turma> listaTurma) {
        Turma t1, t2, t3;
        t1 = new Turma();
        t2 = new Turma();
        t3 = new Turma();
        
        t1.setCodigo(124);
        t1.setDescricao("1o semestre");
        t2.setCodigo(215);
        t2.setDescricao("2o semestre");
        t3.setCodigo(375);
        t3.setDescricao("5o semestre");
        
        listaTurma.add(t1);
        listaTurma.add(t2);
        listaTurma.add(t3);
    }
    
    public static void cadCurso(ArrayList<Curso> listaCurso) {
        Curso c1, c2, c3;
        c1 = new Curso();
        c2 = new Curso();
        c3 = new Curso();
        
        c1.setCodCurso(24);
        c1.setNome("Informática");
        c1.setCargaHoraria(3780);
        c2.setCodCurso(75);
        c2.setNome("Eletrotécnica");
        c2.setCargaHoraria(3900);
        c3.setCodCurso(785);
        c3.setNome("Sistemas para Internet");
        c3.setCargaHoraria(4200);

        listaCurso.add(c1);
        listaCurso.add(c2);
        listaCurso.add(c3);
    }
    
}