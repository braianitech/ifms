package aula08_arraylist;

import java.util.ArrayList;

public class Turma {

    private int codigo;
    private String descricao;
    private ArrayList<Aluno> Alunos;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ArrayList<Aluno> getAlunos() {
        return Alunos;
    }

    public void setAlunos(ArrayList<Aluno> Alunos) {
        this.Alunos = Alunos;
    }

    @Override
    public String toString() {
        return "Curso: " + descricao + ", Código: " + codigo;
    }    
}
