package Exercicio4;

public class Gerente extends Pessoa {

    int codFuncional;

    public int getCodFuncional() {
        return codFuncional;
    }

    public void setCodFuncional(int codFuncional) {
        this.codFuncional = codFuncional;
    }
    
    @Override
    public String toString(){
        return "Nome: "+nome+", cod. funcional: "+codFuncional+", idade: "+idade+", endereço: "+endereco
                +", telefone: "+telefone+", cidade: "+cidade+", estado: "+estado;
    }
}
