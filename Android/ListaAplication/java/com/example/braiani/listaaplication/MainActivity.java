package com.example.braiani.listaaplication;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
    private static final String[] VEICULOS = new String[] {"Carro", "Moto", "Caminhão"};
    ArrayAdapter<String> meuAdaptador;
    final String[] modelos = {"Fiat", "Ford", "Renault", "Honda", "Yamaha", "Suzuki", "Volvo", "Merceds-Benz", "Scania"};
    String itemMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        registerForContextMenu(getListView());

        meuAdaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, VEICULOS);
        setListAdapter(meuAdaptador);
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        itemMenu = (String) getListView().getItemAtPosition(info.position);

        //getMenuInflater().inflate(R.menu.menu_contexto, menu);

        criarMenu(menu, v, itemMenu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Log.d("Position", String.valueOf(info.position));
        String nomeSelecionado = (String) getListView().getItemAtPosition(info.position);

        Toast.makeText(this, "Modelo: " + modelos[item.getItemId()] + " - Categoria: " + nomeSelecionado, Toast.LENGTH_SHORT).show();

        return super.onContextItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int posicao, long id) {
        super.onListItemClick(l, v, posicao, id);
        Toast.makeText(this, "Você selecionou a categoria: " + VEICULOS[posicao], Toast.LENGTH_LONG).show();
    }

    public void criarMenu(ContextMenu menu, View v, String item){
        if(item.equals("Carro")){
            menu.setHeaderTitle("Carros");
            menu.setHeaderIcon(R.drawable.car_icon);
            menu.add(0, 0, 0, "Fiat");
            menu.add(0, 1, 0, "Ford");
            menu.add(0, 2, 0, "Renault");
            return;
        }
        if(item.equals("Moto")){
            menu.setHeaderTitle("Motos");
            menu.setHeaderIcon(R.drawable.moto_icon);
            menu.add(0, 3, 0, "Honda");
            menu.add(0, 4, 0, "Yamaha");
            menu.add(0, 5, 0, "Suzuki");
            return;
        }
        if (item.equals("Caminhão")){
            menu.setHeaderTitle("Caminhão");
            menu.setHeaderIcon(R.drawable.caminhao_icon);
            menu.add(0, 6, 0, "Volvo");
            menu.add(0, 7, 0, "Merceds-Benz");
            menu.add(0, 8, 0, "Scania");
        }
    }
}
