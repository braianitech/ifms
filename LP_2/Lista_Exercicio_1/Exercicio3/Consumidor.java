package Exercicio3;


public class Consumidor {
    int idConsumidor;
    String nome, endereco;

    public int getIdConsumidor() {
        return idConsumidor;
    }

    public void setIdConsumidor(int idConsumidor) {
        this.idConsumidor = idConsumidor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    @Override
    public String toString(){
        return "Id Cliente: "+idConsumidor+", nome: "+ nome+" e endereço: "+ endereco;
    }
    
}
