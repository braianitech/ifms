package bancobraiani;

import java.util.ArrayList;
import java.util.Scanner;

public class BancoBraiani {

    public static void main(String[] args) {
       Cliente cliente = new Cliente();       
       ArrayList<CartaoCredito> ListaCartao = new ArrayList<>();
       
       Scanner entrada = new Scanner(System.in);
       
        System.out.println("<----- CADASTRANDO CLIENTE ----->");
        System.out.println("Digite o nome do cliente: ");
        cliente.setNome(entrada.next());
        
        System.out.println("Digite o id do cliente: ");
        cliente.setId(entrada.nextInt());
        
        System.out.println("<----- CADASTRANDO CARTÕES ----->");
        for(int i=0; i < 3; i++){
            CartaoCredito cartaoCredito = new CartaoCredito();
            System.out.println("Digite o número do "+ (i+1) +"º cartão de crédito: ");
            cartaoCredito.setNumero(entrada.nextInt());
        
            System.out.println("Digite o ano de vencimento do "+ (i+1) +"º cartão de crédito: ");
            cartaoCredito.setValidadeAno(entrada.next());
        
            ListaCartao.add(cartaoCredito);
        }
        cliente.setCartaoCredito(ListaCartao);
        
        System.out.println("O cliente "+ cliente.getNome() +" possui os seguintes cartões: "+ cliente.getCartaoCredito().toString());
    }
    
}
