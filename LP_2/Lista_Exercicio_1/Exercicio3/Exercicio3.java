package Exercicio3;

import java.util.ArrayList;
import java.util.Scanner;


public class Exercicio3 {
    static Scanner entrada = new Scanner(System.in);
    public static void main(String[] args) {
        Software soft1 = new Software();
        Software soft2 = new Software();
        Hardware hard1 = new Hardware();
        Hardware hard2 = new Hardware();
        Consumidor c1 = new Consumidor();
        Consumidor c2 = new Consumidor();
        Consumidor c3 = new Consumidor();
        Pedido p1 = new Pedido();
        Pedido p2 = new Pedido();
        Pedido p3 = new Pedido();
        ArrayList<Produto> listaProduto = new ArrayList<>();
        
        //Cadastros manuais! Pode ser feito por aqui?
        System.out.println("---- Cadastrar produtos ----");
        System.out.println(" --- Cadastrar softwares ---");
        cadastrarSoftware(soft1, 1);
        cadastrarSoftware(soft2, 2);
        System.out.println(" --- Cadastrar hardwares ---");
        cadastrarHardware(hard1, 1);
        cadastrarHardware(hard2, 2);
        //System.out.println(soft1.getDescricao()+" - "+soft2.getDescricao());
        System.out.println("---- Fim cadastro produtos ----");
        
        System.out.println("---- Cadastrar cliente ----");
        cadastrarConsumidor(c1, 1);
        cadastrarConsumidor(c2, 2);
        cadastrarConsumidor(c3, 3);
        System.out.println("---- Fim cadastro cliente ----");
       
        //Gerando os vínculos de pedidos!!!
        //Primeiro pedido
        listaProduto = new ArrayList<>();
        p1.setConsumidor(c1);
        p1.setNumPedido(1);
        p1.setData("13/04/2017");
        listaProduto.add(soft1);
        listaProduto.add(soft2);
        p1.setListaProdutos(listaProduto);
        
        //Segundo pedido
        listaProduto = new ArrayList<>();
        p2.setConsumidor(c2);
        p2.setNumPedido(2);
        p2.setData("14/04/2017");
        listaProduto.add(soft1);
        listaProduto.add(soft2);
        listaProduto.add(hard1);
        listaProduto.add(hard2);
        p2.setListaProdutos(listaProduto);
        
        //Primeiro pedido
        listaProduto = new ArrayList<>();
        p3.setConsumidor(c3);
        p3.setNumPedido(3);
        p3.setData("15/04/2017");
        listaProduto.add(hard1);
        listaProduto.add(hard2);
        p3.setListaProdutos(listaProduto);
        
        //Gerando os relatórios!
        exibirRelatorio(p1, 1);
        exibirRelatorio(p2, 2);
        exibirRelatorio(p3, 3);
    }
    
    public static void cadastrarSoftware(Software software, int num){
        System.out.println("Digite o id do "+ num +"º software: ");
        software.setIdProduto(entrada.nextInt());
        System.out.println("Digite a descrição no "+ num +"º software: ");
        software.setDescricao(entrada.next());
        System.out.println("Digite o preço por unidade do "+ num +"º software: ");
        software.setPrecoUnidade(entrada.nextInt());
        System.out.println("Digite a versão do "+ num +"º software: ");
        software.setNumVersao(entrada.nextInt());
    }
    
    public static void cadastrarHardware(Hardware hardware, int num){
        System.out.println("Digite o id do "+ num +"º hardwares: ");
        hardware.setIdProduto(entrada.nextInt());
        System.out.println("Digite a descrição no "+ num +"º hardwares: ");
        hardware.setDescricao(entrada.next());
        System.out.println("Digite o preço por unidade do "+ num +"º hardwares: ");
        hardware.setPrecoUnidade(entrada.nextInt());
        System.out.println("Digite a descrição da peça do "+ num +"º hardwares: ");
        hardware.setDescricaoPecas(entrada.next());
    }
    
    public static void cadastrarConsumidor(Consumidor consumidor, int num){
        System.out.println("Digite o id do "+ num +"º consumidor: ");
        consumidor.setIdConsumidor(entrada.nextInt());
        System.out.println("Digite o nome do "+ num +"º consumidor: ");
        consumidor.setNome(entrada.next());
        System.out.println("Digite o endereço do "+ num +"º consumidor: ");
        consumidor.setEndereco(entrada.next());
    }
    
    public static void exibirRelatorio(Pedido pedido, int num){
        System.out.println("---- Informações sobre o "+ num +"º pedido ----");
        System.out.println("O "+ num +"º pedido tem o seguinte número: "+pedido.getNumPedido()
                + ", foi comprado pelo consumidor:");
        System.out.println(pedido.getConsumidor().toString());
        System.out.println("Esse pedido contem os seguintes produtos: ");
        System.out.println(pedido.getListaProdutos().toString());
        
    }
}
