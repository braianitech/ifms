package aula_12;
/**
 * @author braiani Criado em 29/05/2017
 */
public class CarroPasseio extends VeiculoAlugado{
    int capacidadePessoas;

    public CarroPasseio() {
    }

    public CarroPasseio(int capacidadePessoas, int codigo, int valor) {
        super(codigo, valor);
        this.capacidadePessoas = capacidadePessoas;
    }

    public int getCapacidadePessoas() {
        return capacidadePessoas;
    }

    public void setCapacidadePessoas(int capacidadePessoas) {
        this.capacidadePessoas = capacidadePessoas;
    }
    
    @Override
    public String imprimirDados(){
        return "Carro de passeio com capacidade para "+ capacidadePessoas +
                " pessoas e valor de R$ "+ super.getValor();
    }
}
