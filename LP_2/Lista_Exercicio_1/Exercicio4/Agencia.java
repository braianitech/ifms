package Exercicio4;

import java.util.ArrayList;


public class Agencia {
    int codAgencia;
    String nomeAgencia;
    ArrayList<ContaCorrente> arrayContaCorrente;
    ArrayList<ContaPoupanca> arrayContaPoupanca;
    ArrayList<Gerente> arrayGerentes;

    public int getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(int codAgencia) {
        this.codAgencia = codAgencia;
    }

    public String getNomeAgencia() {
        return nomeAgencia;
    }

    public void setNomeAgencia(String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    
    }

    public ArrayList<ContaCorrente> getArrayContaCorrente() {
        return arrayContaCorrente;
    }

    public void setArrayContaCorrente(ArrayList<ContaCorrente> arrayContaCorrente) {
        this.arrayContaCorrente = arrayContaCorrente;
    }

    public ArrayList<ContaPoupanca> getArrayContaPoupanca() {
        return arrayContaPoupanca;
    }

    public void setArrayContaPoupanca(ArrayList<ContaPoupanca> arrayContaPoupanca) {
        this.arrayContaPoupanca = arrayContaPoupanca;
    }

    public ArrayList<Gerente> getArrayGerentes() {
        return arrayGerentes;
    }

    public void setArrayGerentes(ArrayList<Gerente> arrayGerentes) {
        this.arrayGerentes = arrayGerentes;
    }
}
