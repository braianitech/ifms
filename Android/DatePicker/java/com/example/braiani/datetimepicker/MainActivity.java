package com.example.braiani.datetimepicker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    Button btnCalendario;
    private Calendar calendario;
    int ano, mes, dia;
    static final int DIALOG_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calendario = calendario.getInstance();
        ano = calendario.get(calendario.YEAR);
        mes = calendario.get(calendario.MONTH);
        dia = calendario.get(calendario.DAY_OF_MONTH);

        showDialogOnButtonClick();
    }

    @SuppressWarnings("deprecation")
    private void showDialogOnButtonClick() {
        btnCalendario = (Button) findViewById(R.id.btnCalendario);

        btnCalendario.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }

    @Override
    @SuppressWarnings("deprecation")
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_ID) {
            DatePickerDialog dp = new DatePickerDialog(this, dpickerListener, ano, mes, dia);
            dp.getDatePicker().setCalendarViewShown(false);
            dp.getDatePicker().setSpinnersShown(true);
            return dp;
        }else
            return null;
    }

    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            ano = year;
            mes = month;
            dia = dayOfMonth;

            //Exibir
            Toast.makeText(MainActivity.this, dia + "/" + (mes + 1) + "/" + ano, Toast.LENGTH_LONG).show();
        }
    };
}
