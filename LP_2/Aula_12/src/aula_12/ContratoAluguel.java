package aula_12;

import java.util.ArrayList;

/**
 * @author braiani Criado em 29/05/2017
 */
public class ContratoAluguel {
    String data;
    Cliente cliente;
    ArrayList<VeiculoAlugado> veiculoAlugado;

    public ContratoAluguel(String data, Cliente cliente, ArrayList<VeiculoAlugado> veiculoAlugado) {
        this.data = data;
        this.cliente = cliente;
        this.veiculoAlugado = veiculoAlugado;
    }

    public ContratoAluguel() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<VeiculoAlugado> getVeiculoAlugado() {
        return veiculoAlugado;
    }

    public void setVeiculoAlugado(ArrayList<VeiculoAlugado> veiculoAlugado) {
        this.veiculoAlugado = veiculoAlugado;
    }

    @Override
    public String toString() {
//        return "data: " + data + ", cliente: " + cliente.imprimirDados() + ", veiculo alugado: " +
//                veiculoAlugado.toString();
        String retorno = "";
        for (int i = 0; i < veiculoAlugado.size(); i++) {
            retorno = retorno + "data: " + data + ", cliente: " + cliente.imprimirDados() + ", veiculo alugado: " +
               veiculoAlugado.get(i).imprimirDados() + "\n";
        }
        return retorno;
    }
}
