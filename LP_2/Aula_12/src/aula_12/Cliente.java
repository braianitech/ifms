package aula_12;
/**
 * @author braiani Criado em 29/05/2017
 */
public class Cliente {
    String nome, cpf;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public String imprimirDados(){
        return "nome: " + nome + ", CPF: " + cpf;
    }

    public Cliente(String nome, String cpf) {
        this.nome = nome;
        this.cpf = cpf;
    }

    public Cliente() {
    }
    
}
